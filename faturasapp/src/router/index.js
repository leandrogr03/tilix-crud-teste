import Vue from 'vue'
import Router from 'vue-router'
import Sobre from '@/components/Sobre'
import Faturas from '@/components/Faturas'
// import Create from '@/components/Create'
// import Update from '@/components/Update'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Sobre',
      component: Sobre
    },
    {
      path: '/faturas',
      name: 'Faturas',
      component: Faturas
    }
  ]
})
