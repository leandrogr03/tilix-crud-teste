# TESTE CRUD Faturas

Projeto com um CRUD de Faturas com API Rest em NodeJS e Aplicação em Vuejs

### Prerequisites

- docker
- node
- npm
- nodemon
- sequelize
- sequelize-cli
- vue-cli

### Rodando os Projetos

- Instruções para a API

```
cd /faturasapi
```
```
docker-composer up -d
```
```
npm install
```

Gerando as tabelas no banco:

```
sequelize db:migrate
```

Preenchendo banco com dados fake:

```
sequelize db:seed:all
```

Rodando a aplicação:

```
nodemon server.js ou node server.js
```

- Instruções para a Aplicação VueJs

```
cd /faturasapp
```
```
npm install
```

Rodando a aplicação modo Desenvolvimento:

```
npm run dev 
```

Rodando a aplicação modo Produção:

```
npm run buid 
```

Dúvidas ?
|
V

## Authors

* **Leandro Rosa** - (leandrogr03@gmail.com)
