'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.bulkInsert('faturas', [
      {
        iduser: 1,
        nome_empresa: 'Empresa 1',
        valor: 1500,
        data_vencimento: '2019-02-10 00:00:00',
        pagou: 'N',
        createdAt: new Date(),
        updatedAt : new Date()
      },
      {
        iduser: 1,
        nome_empresa: 'Empresa 2',
        valor: 500,
        data_vencimento: '2019-02-11 00:00:00',
        pagou: 'N',
        createdAt: new Date(),
        updatedAt : new Date()
      },
      {
        iduser: 1,
        nome_empresa: 'Empresa 3',
        valor: 2000,
        data_vencimento: '2019-02-12 00:00:00',
        pagou: 'S',
        createdAt: new Date(),
        updatedAt : new Date()
      }
    ], {});

  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('faturas', null, {});
  }
};
