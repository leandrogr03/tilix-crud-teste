'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('faturas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      iduser: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      nome_empresa: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      valor: {
        allowNull: false,
        type: Sequelize.DECIMAL(10,2),
      },
      data_vencimento: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      pagou: {
        allowNull: false,
        type: Sequelize.ENUM('S', 'N'),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('faturas');
  }
};
