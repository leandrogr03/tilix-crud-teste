'use strict';

module.exports = function(sequelize, Sequelize) {
    var Fatura = sequelize.define('fatura', {
        iduser: Sequelize.INTEGER,
        nome_empresa: Sequelize.STRING,
        valor: Sequelize.DECIMAL(10,2),
        data_vencimento: Sequelize.DATE,
        pagou: Sequelize.ENUM('S', 'N')
    }, {});

    // Fatura.associate = function(models) {
    //     Fatura.belongsTo(models.User, { as : 'user', foreignKey: 'iduser' });
    // } 

    return Fatura;
};