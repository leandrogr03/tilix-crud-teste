'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING
    }, {});

    // User.associate = function(models){
    //     User.hasMany(models.Fatura, {as: 'faturas', foreignKey: 'iduser'})
    // }

    return User;
};