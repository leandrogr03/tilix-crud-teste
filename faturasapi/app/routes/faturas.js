var express = require('express');
var Fatura = require('../models').fatura;
var router = express.Router();
var moment = require('moment');
var Sequelize = require('sequelize');

router.get('/', function(req, res) {
    console.log('listando todas as faturas');
    Fatura.findAll({
        attributes: [
            'id',
            'nome_empresa',
            'valor',
            [Sequelize.fn('date_format', Sequelize.col('data_vencimento'), '%d/%m/%Y'), 'data_vencimento'],
            'pagou'
        ]
    }).then(faturas => {
        res.json(faturas)
    });
});

router.get('/:id', function(req, res){
    console.log('pegar uma fatura');
    Fatura.findById(req.params.id).then(fatura => {
        res.json(fatura);
    });
});

router.post('/', function(req, res){
    Fatura.create({
        iduser: req.body.iduser,
        nome_empresa: req.body.nome_empresa,
        valor: req.body.valor, 
        data_vencimento: req.body.data_vencimento,
        pagou: req.body.pagou
    }).then(fatura => {
        console.log(fatura.get({
          plain: true
        }));
        res.send(fatura);
    });
});

router.put('/:id', function(req, res){
    Fatura.update({
        iduser: req.body.iduser,
        nome_empresa: req.body.nome_empresa,
        valor: req.body.valor, 
        data_vencimento: req.body.data_vencimento,
        pagou: req.body.pagou
    },{ 
        where: { id: req.params.id } 
    }).then(result => {
        res.status(200).json(result);
    });
});

router.delete('/:id', function(req, res){
    Fatura.destroy({ 
        where: { id: req.params.id } 
    }).then(result => {
        res.status(200).json(result);
    });
});

module.exports = router;