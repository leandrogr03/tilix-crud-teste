const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

// routes
var faturas = require('./app/routes/faturas');
// models
var models = require("./app/models");

//Sync Database
// models.sequelize.sync().then(function() {
//   console.log('connected to database')
// }).catch(function(err) {
//   console.log(err)
// });

const app = express();

// Habilitar o CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// registrar routes
app.use('/faturas', faturas);
// app.use('/users', users);

app.get('/', (req, res) => {
  res.send('API Tilix teste rodando...'); 
});

app.listen(3000);